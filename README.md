
lua-LIVR
========

Introduction
------------

This module implements a lightweight validator supporting Language Independent Validation Rules Specification 2.0 (LIVR).

The documentation of LIVR is available on <https://livr-spec.org/>.

More rules are available with <https://fperrad.frama.io/lua-LIVR-extra>.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-LIVR>,
and the sources are hosted at <https://framagit.org/fperrad/lua-LIVR>.

Copyright and License
---------------------

Copyright (c) 2018-2024 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

