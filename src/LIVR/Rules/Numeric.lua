
--
-- lua-LIVR : <https://fperrad.frama.io/lua-LIVR/>
--

if _VERSION < 'Lua 5.3' then
    require'compat53'
end
local expected_type = require'LIVR.helpers'.expected_type
local string_number_type = require'LIVR.helpers'.string_number_type
local tonumber = tonumber
local type = type
local tointeger = math.tointeger
local _ENV = nil

return {
    integer = function ()
        return function (value)
            if value ~= nil and value ~= '' then
                if not string_number_type[type(value)] then
                    return value, 'FORMAT_ERROR'
                end
                local num = tointeger(tonumber(value))
                if num == nil then
                    return value, 'NOT_INTEGER'
                end
                value = num
            end
            return value
        end
    end,

    positive_integer = function ()
        return function (value)
            if value ~= nil and value ~= '' then
                if not string_number_type[type(value)] then
                    return value, 'FORMAT_ERROR'
                end
                local num = tointeger(tonumber(value))
                if num == nil or num <= 0 then
                    return value, 'NOT_POSITIVE_INTEGER'
                end
                value = num
            end
            return value
        end
    end,

    decimal = function ()
        return function (value)
            if value ~= nil and value ~= '' then
                if not string_number_type[type(value)] then
                    return value, 'FORMAT_ERROR'
                end
                local num = tonumber(value)
                if num == nil then
                    return value, 'NOT_DECIMAL'
                end
                value = num
            end
            return value
        end
    end,

    positive_decimal = function ()
        return function (value)
            if value ~= nil and value ~= '' then
                if not string_number_type[type(value)] then
                    return value, 'FORMAT_ERROR'
                end
                local num = tonumber(value)
                if num == nil or num <= 0 then
                    return value, 'NOT_POSITIVE_DECIMAL'
                end
                value = num
            end
            return value
        end
    end,

    max_number = function (_, max_number)
        expected_type('max_number', 1, 'number', max_number)
        return function (value)
            if value ~= nil and value ~= '' then
                if not string_number_type[type(value)] then
                    return value, 'FORMAT_ERROR'
                end
                local num = tonumber(value)
                if num == nil then
                    return value, 'NOT_NUMBER'
                end
                if num > max_number then
                    return value, 'TOO_HIGH'
                end
                value = num
            end
            return value
        end
    end,

    min_number = function (_, min_number)
        expected_type('min_number', 1, 'number', min_number)
        return function (value)
            if value ~= nil and value ~= '' then
                if not string_number_type[type(value)] then
                    return value, 'FORMAT_ERROR'
                end
                local num = tonumber(value)
                if num == nil then
                    return value, 'NOT_NUMBER'
                end
                if num < min_number then
                    return value, 'TOO_LOW'
                end
                value = num
            end
            return value
        end
    end,

    number_between = function (_, min_number, max_number)
        expected_type('number_between', 1, 'number', min_number)
        expected_type('number_between', 2, 'number', max_number)
        return function (value)
            if value ~= nil and value ~= '' then
                if not string_number_type[type(value)] then
                    return value, 'FORMAT_ERROR'
                end
                local num = tonumber(value)
                if num == nil then
                    return value, 'NOT_NUMBER'
                end
                if num < min_number then
                    return value, 'TOO_LOW'
                end
                if num > max_number then
                    return value, 'TOO_HIGH'
                end
                value = num
            end
            return value
        end
    end,
}

--
-- Copyright (c) 2018-2024 Francois Perrad
--
-- This library is licensed under the terms of the MIT/X11 license,
-- like Lua itself.
--
