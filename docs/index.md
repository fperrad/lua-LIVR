
# lua-LIVR

---

## Overview

lua-LIVR is a lightweight validator supporting Language Independent Validation Rules Specification (LIVR).

Features:

- Rules are declarative and language independent
- Any number of rules for each field
- Return together errors for all fields
- Excludes all fields that do not have validation rules described
- Has possibility to validatate complex hierarchical structures
- Easy to describe and understand rules
- Returns understandable error codes (not error messages)
- Easy to add own rules
- Multipurpose (user input validation, configs validation, contracts programming, etc)

## References

The LIVR specifications are available on <https://livr-spec.org/>.

## Status

lua-LIVR is now stable.

It's developed for Lua 5.1, 5.2, 5.3 & 5.4.

## Download

lua-LIVR source can be downloaded from
[Framagit](https://framagit/fperrad/lua-LIVR).

The Teal type definition of this library is available
[here](https://github.com/teal-language/teal-types/blob/master/types/lua-livr/LIVR.d.tl).

## Installation

With Lua 5.1 & 5.2, lua-LIVR depends on [compat53](https://github.com/lunarmodules/lua-compat-5.3).

lua-LIVR have two optional dependencies
[lua-utf8](https://github.com/starwing/luautf8) &
[Lrexlib-PCRE](https://rrthomas.github.io/lrexlib/).

lua-LIVR is available via LuaRocks:

```sh
luarocks install lua-livr
```

lua-LIVR is available via opm:

```
opm get fperrad/lua-livr
```

or manually, with:

```sh
make install
```

## Test

The test suite requires the modules
[lua-TestAssertion](https://fperrad.frama.io/lua-testassertion/),
[dkjson](http://dkolf.de/src/dkjson-lua.fsl/home) &
[LuaFileSystem](https://keplerproject.github.io/luafilesystem/).

    make test

## Copyright and License

Copyright &copy; 2018-2024 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.
