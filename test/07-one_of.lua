#!/usr/bin/env lua

require 'Test.Assertion'

plan(14)

local livr = require 'LIVR'

local validator = livr.new({
    str1        = { one_of = { '1', '10', '100' } },
    str2        = { 'string', one_of = { '1', '10', '100' } },
    int1        = { one_of = { 1, 10, 100 } },
    int2        = { 'integer', one_of = { 1, 10, 100 } },
    dec1        = { one_of = { 0.1, 0.01, 0.001 } },
    dec2        = { 'decimal', one_of = { 0.1, 0.01, 0.001 } },
}, true)
truthy( validator )

local data = validator:validate{
    str1        = '1',
    str2        = '10',
    int1        = '1',
    int2        = '10',
    dec1        = '0.1',
    dec2        = '0.01',
}
truthy( data )
is_string( data.str1, "one_of string" )
equals( data.str1, '1' )
is_string( data.str2, "one_of string with useless casted" )
equals( data.str2, '10' )
is_number( data.int1, "one_of integer" )
equals( data.int1, 1 )
is_number( data.int2, "one_of integer with useless casted" )
equals( data.int2, 10 )
is_number( data.dec1, "one_of decimal" )
equals( data.dec1, 0.1 )
is_number( data.dec2, "one_of decimal with useless casted" )
equals( data.dec2, 0.01 )

