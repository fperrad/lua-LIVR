#!/usr/bin/env lua

require 'Test.Assertion'

plan(9)

local livr = require 'LIVR'

livr.register_default_rules{
    strong_password = function ()
        return function (value)
            if value ~= nil and value ~= '' then
                if type(value) == 'number' then
                    value = tostring(value)
                end
                if type(value) ~= 'string' then
                    return value, 'FORMAT_ERROR'
                end
                if #value < 6 then
                    return value, 'WEAK_PASSWORD'
                end
            end
            return value
        end
    end
}

local validator = livr.new{
    code            = 'alphanumeric',
    password        = 'strong_password',
    address         = { nested_object  = {
        street      = 'alphanumeric',
        password    = 'strong_password'
    } }
}
truthy( validator )

validator:register_rules{
    alphanumeric = function ()
        return function (value)
            if value ~= nil and value ~= '' then
                if type(value) == 'number' then
                    value = tostring(value)
                end
                if type(value) ~= 'string' then
                    return value, 'FORMAT_ERROR'
                end
                if not value:match'^[a-z0-9]+$' then
                    return value, 'NOT_ALPHANUMERIC'
                end
            end
            return value
        end
    end
}

is_function( livr.default_rules.strong_password, "Default rules should contain 'strong_password' rule" )
is_nil( livr.default_rules.alphanumeric, "Default rules should not contain 'alphanumeric' rule" )

is_function( validator:get_rules().strong_password, "Validator rules should contain 'strong_password' rule" )
is_function( validator:get_rules().alphanumeric, "Validator rules should contain 'alphanumeric' rule" )

local data, err = validator:validate{
    code            = '!qwe',
    password        = 123,
    address         = {
        street      = 'Some Street!',
        password    = 'qwer'
    }
}
falsy( data, "should return false due to validation errors" )
same( err, {
    code            = 'NOT_ALPHANUMERIC',
    password        = 'WEAK_PASSWORD',
    address         = {
        street      = 'NOT_ALPHANUMERIC',
        password    = 'WEAK_PASSWORD'
    }
}, "should contain error codes" )

error_matches( function ()
    livr.register_default_rules{
        strong_password = 'foo'
    }
end, "^[^:]+:%d+: RULE_BUILDER %[strong_password%] SHOULD BE A FUNCTION" )

error_matches( function ()
    validator:register_rules{
        alphanumeric = 'bar'
    }
end, "^[^:]+:%d+: RULE_BUILDER %[alphanumeric] SHOULD BE A FUNCTION" )
