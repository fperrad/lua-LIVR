#!/usr/bin/env lua

require 'Test.Assertion'

plan(5)

local livr = require 'LIVR'
is_function( livr.default_rules.like_lua )
livr.default_rules.like = livr.default_rules.like_lua

local validator = livr.new{
    first_name  = { like = "^%a+$" },
    last_name   = { like = "^%w+$" },
    age         = { like = "^[1-9][0-9]*$" },
}
truthy( validator )

local data, err = validator:validate{
    first_name  = 'Fran3ois',
    last_name   = 'Perrad ',
    age         = { 51 },
}
is_nil( data )
same( err, {
    first_name  = 'WRONG_FORMAT',
    last_name   = 'WRONG_FORMAT',
    age         = 'FORMAT_ERROR',
} )

data = validator:validate{
    first_name  = 'Francois',
    last_name   = 'Perrad',
    age         = 51,
}
truthy( data )

