#!/usr/bin/env lua

require 'Test.Assertion'

plan(25)

if not require_ok 'LIVR' then
    BAIL_OUT "no lib"
end

local m = require 'LIVR'
is_table( m, 'module' )
equals( m, package.loaded['LIVR'], 'package.loaded' )

is_table( m.default_rules, 'default_rules' )
is_boolean( m.default_auto_trim, 'default_auto_trim' )
is_false( m.default_auto_trim, 'default_auto_trim' )
is_function( m.new, 'new' )
is_function( m.register_default_rules, 'register_default_rules' )
is_function( m.register_aliased_default_rule, 'register_aliased_default_rule' )

local o = m.new{}
is_table( o, 'instance' )
is_function( o.prepare, 'meth prepare' )
is_function( o.validate, 'meth validate' )
is_function( o.register_rules, 'meth register_rules' )
is_function( o.register_aliased_rule, 'meth register_aliased_rule' )
is_function( o.get_rules, 'meth get_rules' )

equals( m._NAME, 'LIVR', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'validator supporting LIVR', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

local h = require 'LIVR.helpers'
is_table( h, 'module' )
equals( h, package.loaded['LIVR.helpers'], 'package.loaded' )

is_table( h.primitive_type, 'primitive_type' )
is_table( h.string_number_type, 'string_number_type' )
is_table( h.number_boolean_type, 'number_boolean_type' )
