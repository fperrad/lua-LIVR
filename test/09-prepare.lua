#!/usr/bin/env lua

require 'Test.Assertion'

plan(6)

local livr = require 'LIVR'
local validator

validator = livr.new({
    code            = 'required',
    password        = { 'required', { min_length = 3 } },
    address         = { nested_object  = {
        street  = { min_length = 5 },
    } }
}, true)
truthy( validator )
truthy( validator:prepare() )

validator = livr.new({
    code            = 'required',
    password        = { 'required', { min_length = 3 } },
    address         = { nested__object  = {
        street  = { min_length = 5 },
    } }
}, true)
truthy( validator )
error_matches( function () validator:prepare() end,
               "^[^:]+:%d+: Rule %[nested__object%] not registered" )

validator = livr.new({
    code            = 'required',
    password        = { 'required', { min_length = 'foo' } },
    address         = { nested_object  = {
        street  = { min_length = 5 },
    } }
}, true)
truthy( validator )
error_matches( function () validator:prepare() end,
               "^[^:]+:%d+: Rule %[min_length%] bad argument #1 %(number expected%)" )
