#!/usr/bin/env lua

require 'Test.Assertion'

plan(11)

local livr = require 'LIVR'

livr.register_aliased_default_rule{
    name  = 'strong_password',
    rules = { min_length = 6 },
    error = 'WEAK_PASSWORD',
}

local validator = livr.new{
    code            = 'alphanumeric',
    password        = 'strong_password',
    address         = { nested_object  = {
        street      = 'alphanumeric',
        password    = 'strong_password'
    } }
}
truthy( validator )

validator:register_aliased_rule{
    name  = 'alphanumeric',
    rules = { like_lua = '^[a-z0-9]+$' },
    error = 'NOT_ALPHANUMERIC',
}

is_function( livr.default_rules.strong_password, "Default rules should contain 'strong_password' rule" )
is_nil( livr.default_rules.alphanumeric, "Default rules should not contain 'alphanumeric' rule" )

is_function( validator:get_rules().strong_password, "Validator rules should contain 'strong_password' rule" )
is_function( validator:get_rules().alphanumeric, "Validator rules should contain 'alphanumeric' rule" )

local data, err = validator:validate{
    code            = '!qwe',
    password        = 123,
    address         = {
        street      = 'Some Street!',
        password    = 'qwer'
    }
}
falsy( data, "should return false due to validation errors" )
same( err, {
    code            = 'NOT_ALPHANUMERIC',
    password        = 'WEAK_PASSWORD',
    address         = {
        street      = 'NOT_ALPHANUMERIC',
        password    = 'WEAK_PASSWORD'
    }
}, "should contain error codes" )

error_matches( function ()
    livr.register_aliased_default_rule{
        name  = 'strong_password',
        rule  = { min_length = 6 },
    }
end, "^[^:]+:%d+: Alias rules required" )

error_matches( function ()
    livr.register_aliased_default_rule{
        alias = 'strong_password',
        rules = { min_length = 6 },
    }
end, "^[^:]+:%d+: Alias name required" )

error_matches( function ()
    validator:register_aliased_rule{
        name  = 'alphanumeric',
        rule  = { like_lua = '^[a-z0-9]+$' },
    }
end, "^[^:]+:%d+: Alias rules required" )

error_matches( function ()
    validator:register_aliased_rule{
        alias = 'alphanumeric',
        rules = { like_lua = '^[a-z0-9]+$' },
    }
end, "^[^:]+:%d+: Alias name required" )
