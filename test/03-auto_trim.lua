#!/usr/bin/env lua

require 'Test.Assertion'

plan(4)

local livr = require 'LIVR'

local validator = livr.new({
    code            = 'required',
    password        = { 'required', { min_length = 3 } },
    address         = { nested_object  = {
        street  = { min_length = 5 },
    } }
}, true)
truthy( validator )

local data, err = validator:validate{
    code        = '  ',
    password    = ' 12  ',
    address     = {
        street  = '  hell '
    }
}
falsy( data, "return false due to validation errors for trimmed values" )
same( err, {
    code        = 'REQUIRED',
    password    = 'TOO_SHORT',
    address     = {
        street  = 'TOO_SHORT',
    }
}, "should contain error codes" )

data = validator:validate{
    code        = ' A ',
    password    = ' 123  ',
    address     = {
        street  = '  hello '
    }
}
same( data, {
    code        = 'A',
    password    = '123',
    address     = {
        street  = 'hello',
    }
}, "should contain trimmed data" )

